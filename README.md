# Collection `labotekh.labotekh`

This collection aim to store all roles and playbooks used to deploy projects on [labotekh.fr](https://www.labotekh.fr).

To use this colection roles please read each role documentation and provide the following variables in the `deployment.yml` playbook. You can put them in the `vars/labotekh.yml` file which is read by default by the playbook.

```yml
certbot_email: example@test.com

default_admin_username: username
default_admin_password: ***************

gitlab_registry: xxxx.xxxx.xx                           # ex : registry.gitlab.com
gitlab_username: username
gitlab_token: **************

root_password: ****************
ansible_password: ******************
runner_password: *******************

backup_key: *************                               # used to encrypt backup data with AES256

root_directory: dirpath                                 # ex : /labotekh
reverse_proxy_dirname: dirname                          # ex : _front

ovh:                                                    # these 5 lines can be commented to disable HTTPS
  dns_ovh_endpoint: xxx-xx
  dns_ovh_application_key: *************
  dns_ovh_application_secret: **********
  dns_ovh_consumer_key: *************

s3:                                                     # these 6 lines can be commented to disable backup
  url : https://xx.xxxxx.xx
  region: xx-xxx                                        # ex : fr-par
  bucket: bucket-name
  access_key: ***************
  secret_key: ***************
```

After running all roles, the directory structure on each  host will be like so :
```
<root_directory>
├── <reverse_proxy_dirname>             # reverse proxy configuration
├── default                             # default website for www.<root_domain>
└── <service_name>
    ├── backup_restore.sh               # restore script
    ├── backup_save.sh                  # backup script
    ├── data                            # postgres database
    │   └── <service_name> 
    ├── docker-compose.yml
    ├── files                           # location of static files, media and logs
    └── <service_name>.env              # service environment variables
```

You also need to set some variables at the host level in the inventory like so :
```
<hostname> ansible_port=XXXX root_domain=xxxx.xx ansible_host=1.1.1.1 enabled_services='["<service_name>","<service_name>",...]'
```
