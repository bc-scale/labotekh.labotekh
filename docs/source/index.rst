


.. _plugins_in_labotekh.labotekh:

Labotekh.Labotekh
=================

Collection version 0.0.6

.. contents::
   :local:
   :depth: 1

Description
-----------

This collection provide all roles used to deploy multiple web apps behind a nginx reverse proxy with fqdn and OVH dns managment

**Author:**

* Imotekh (imotekh@protonmail.com)

**Supported ansible-core versions:**

* 2.12.0 or newer

.. raw:: html

  <p class="ansible-links">
    <a href="https://gitlab.com/labotekh/labotekh/labotekh.labotekh/-/issues" aria-role="button" target="_blank" rel="noopener external">Issue Tracker</a>
    <a href="https://labotekh.fr" aria-role="button" target="_blank" rel="noopener external">Homepage</a>
    <a href="https://gitlab.com/labotekh/labotekh/labotekh.labotekh" aria-role="button" target="_blank" rel="noopener external">Repository (Sources)</a>
  </p>



.. toctree::
    :maxdepth: 1


Plugin Index
------------

These are the plugins in the labotekh.labotekh collection:


Modules
~~~~~~~

* :ref:`backup_policy module <ansible_collections.labotekh.labotekh.backup_policy_module>` -- Filter files to be deleted according to backup policy.

.. toctree::
    :maxdepth: 1
    :hidden:

    backup_policy_module
