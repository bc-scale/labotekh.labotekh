
.. Document meta

:orphan:

.. |antsibull-internal-nbsp| unicode:: 0xA0
    :trim:

.. role:: ansible-attribute-support-label
.. role:: ansible-attribute-support-property
.. role:: ansible-attribute-support-full
.. role:: ansible-attribute-support-partial
.. role:: ansible-attribute-support-none
.. role:: ansible-attribute-support-na
.. role:: ansible-option-type
.. role:: ansible-option-elements
.. role:: ansible-option-required
.. role:: ansible-option-versionadded
.. role:: ansible-option-aliases
.. role:: ansible-option-choices
.. role:: ansible-option-choices-default-mark
.. role:: ansible-option-default-bold
.. role:: ansible-option-configuration
.. role:: ansible-option-returned-bold
.. role:: ansible-option-sample-bold

.. Anchors

.. _ansible_collections.labotekh.labotekh.backup_policy_module:

.. Anchors: short name for ansible.builtin

.. Anchors: aliases



.. Title

labotekh.labotekh.backup_policy
+++++++++++++++++++++++++++++++

.. Collection note

.. note::
    This module is part of the `labotekh.labotekh collection <https://galaxy.ansible.com/labotekh/labotekh>`_ (version 0.0.6).

    To install it, use: :code:`ansible-galaxy collection install labotekh.labotekh`.

    To use it in a playbook, specify: :code:`labotekh.labotekh.backup_policy`.

.. version_added

.. rst-class:: ansible-version-added

New in labotekh.labotekh 0.0.5

.. contents::
   :local:
   :depth: 1

.. Deprecated


Synopsis
--------

.. Description

- Take as input a list of backup files named as "<previous_path>/\<pattern\>" and return files to be deleted according to backup policy configured in the task.


.. Aliases


.. Requirements






.. Options

Parameters
----------


.. rst-class:: ansible-option-table

.. list-table::
  :width: 100%
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Comments

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-dayly"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__parameter-dayly:

      .. rst-class:: ansible-option-title

      **dayly**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-dayly" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`integer`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Number of dayly backup saved.


      .. rst-class:: ansible-option-line

      :ansible-option-default-bold:`Default:` :ansible-option-default:`7`

      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-files"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__parameter-files:

      .. rst-class:: ansible-option-title

      **files**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-files" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`list` / :ansible-option-elements:`elements=string` / :ansible-option-required:`required`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      A list of backup files to filter for deletion. Each file must be named as "<previous_path>/\<pattern\>"


      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-monthly"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__parameter-monthly:

      .. rst-class:: ansible-option-title

      **monthly**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-monthly" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`integer`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Number of monthly backup saved.


      .. rst-class:: ansible-option-line

      :ansible-option-default-bold:`Default:` :ansible-option-default:`6`

      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-pattern"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__parameter-pattern:

      .. rst-class:: ansible-option-title

      **pattern**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-pattern" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Date string format used to name backup files. It must at least define the day and the extension and not contains / (ex "%Y-%m-%d.tgz").


      .. rst-class:: ansible-option-line

      :ansible-option-default-bold:`Default:` :ansible-option-default:`"%Y-%m-%d.tgz"`

      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-weekly"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__parameter-weekly:

      .. rst-class:: ansible-option-title

      **weekly**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-weekly" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`integer`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Number of weekly backup saved.


      .. rst-class:: ansible-option-line

      :ansible-option-default-bold:`Default:` :ansible-option-default:`4`

      .. raw:: html

        </div>

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="parameter-yearly"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__parameter-yearly:

      .. rst-class:: ansible-option-title

      **yearly**

      .. raw:: html

        <a class="ansibleOptionLink" href="#parameter-yearly" title="Permalink to this option"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`integer`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Number of yearly backup saved.


      .. rst-class:: ansible-option-line

      :ansible-option-default-bold:`Default:` :ansible-option-default:`1`

      .. raw:: html

        </div>


.. Attributes


.. Notes


.. Seealso


.. Examples

Examples
--------

.. code-block:: yaml

    
    - name: Extract files to be deleted
      backup_policy:
        dayly: 3
        weekly: 4
        monthly: 1
        yearly: 0
        pattern: "%d-%m-%Y.tgz"
        files:
          - /myapp/08-08-2022.tgz
          - /myapp/07-08-2022.tgz
          - /myapp/06-08-2022.tgz
          - /myapp/05-08-2022.tgz
          - /myapp/05-08-2022.tgz
          - /myapp/01-08-2022.tgz
          - /myapp/25-07-2022.tgz
          - /myapp/18-07-2022.tgz




.. Facts


.. Return values

Return Values
-------------
Common return values are documented `here <https://docs.ansible.com/ansible/latest/reference_appendices/common_return_values.html#common-return-values>`_, the following are the fields unique to this module:

.. rst-class:: ansible-option-table

.. list-table::
  :width: 100%
  :widths: auto
  :header-rows: 1

  * - Key
    - Description

  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-changed"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__return-changed:

      .. rst-class:: ansible-option-title

      **changed**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-changed" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`boolean`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      Describe if there are files to delete or not.


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` always

      .. rst-class:: ansible-option-line
      .. rst-class:: ansible-option-sample

      :ansible-option-sample-bold:`Sample:` :ansible-rv-sample-value:`true`


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-diff"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__return-diff:

      .. rst-class:: ansible-option-title

      **diff**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-diff" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`dictionary`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      The difference of files remaining after deletion


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` always

      .. rst-class:: ansible-option-line
      .. rst-class:: ansible-option-sample

      :ansible-option-sample-bold:`Sample:` :ansible-rv-sample-value:`{"after": ["..."], "before": ["..."]}`


      .. raw:: html

        </div>


  * - .. raw:: html

        <div class="ansible-option-cell">
        <div class="ansibleOptionAnchor" id="return-to_delete"></div>

      .. _ansible_collections.labotekh.labotekh.backup_policy_module__return-to_delete:

      .. rst-class:: ansible-option-title

      **to_delete**

      .. raw:: html

        <a class="ansibleOptionLink" href="#return-to_delete" title="Permalink to this return value"></a>

      .. rst-class:: ansible-option-type-line

      :ansible-option-type:`list` / :ansible-option-elements:`elements=string`

      .. raw:: html

        </div>

    - .. raw:: html

        <div class="ansible-option-cell">

      The list of files to delete.


      .. rst-class:: ansible-option-line

      :ansible-option-returned-bold:`Returned:` always

      .. rst-class:: ansible-option-line
      .. rst-class:: ansible-option-sample

      :ansible-option-sample-bold:`Sample:` :ansible-rv-sample-value:`["/my\_app/XX-XX-XXXX.tgz", "/my\_app/YY-YY-YYYY.tgz"]`


      .. raw:: html

        </div>



..  Status (Presently only deprecated)


.. Authors

Authors
~~~~~~~

- Imotekh 



.. Extra links

Collection links
~~~~~~~~~~~~~~~~

.. raw:: html

  <p class="ansible-links">
    <a href="https://gitlab.com/labotekh/labotekh/labotekh.labotekh/-/issues" aria-role="button" target="_blank" rel="noopener external">Issue Tracker</a>
    <a href="https://labotekh.fr" aria-role="button" target="_blank" rel="noopener external">Homepage</a>
    <a href="https://gitlab.com/labotekh/labotekh/labotekh.labotekh" aria-role="button" target="_blank" rel="noopener external">Repository (Sources)</a>
  </p>

.. Parsing errors

