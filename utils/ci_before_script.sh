#!/bin/sh
whoami
chmod -v 700 $(pwd)

export DEBIAN_FRONTEND=noninteractive # prevent debian from asking user input
export ANSIBLE_HOST_KEY_CHECKING=False # disable host key checking for ansible

# install dependencies
apt-get update -qy
apt install -qy python software-properties-common git
apt-add-repository --yes --update ppa:ansible/ansible
apt install -qy ansible ansible-lint
ansible --version
ansible-lint --version

# setup ansible-vault and SSH key
mkdir secret # create secret dir
echo "$ANSIBLE_SSH_KEY" > secret/ansible.key # create private key from gitlab variable
echo "$ANSIBLE_VAULT_PASSWORD" > secret/ansible_vault_password
chmod -R 400 secret

git submodule deinit --all --force
sed -i "s/https:\/\/gitlab.com\/labotekh\/labotekh\/vars.git/https:\/\/labotekh-labotekh:$CI_ACCESS_VARS_TOKEN@gitlab.com\/labotekh\/labotekh\/vars.git/g" .gitmodules
git submodule init
git submodule update --init

# install requirements
ansible-galaxy collection install -r requirements.yml
