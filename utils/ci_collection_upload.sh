#!/bin/sh
ansible-galaxy collection install labotekh.labotekh
LOCAL_VERSION=$(cat galaxy.yml | grep version: | cut -d ' ' -f 2)
REMOTE_VERSION=$(ansible-galaxy collection list | grep labotekh.labotekh | cut -d ' ' -f 2)
if [ "$LOCAL_VERSION" != "$REMOTE_VERSION" ];
then
	ansible-galaxy collection build
    ansible-galaxy collection publish labotekh-labotekh-* --token $ANSIBLE_COLLECTION_TOKEN
fi
